# Onionspray log file parser

## About

This is an [Onionspray][] webserver log parser written in Python and compatible
with [Enterprise Onion Toolkit (EOTK)][].

[Onionspray]: https://gitlab.torproject.org/tpo/onion-services/onionspray
[Enterprise Onion Toolkit (EOTK)]: https://github.com/alecmuffett/eotk

It can be used to gather basic statistics from instance access but can also be used
as a base for ingesting data into a database.

## How it works

This parser recursively scans a folder looking for [Onionspray][] logs, parsing then
line by line using a regular expression.

It's possible to handle each line both by it's contents and also by the site
(derived from the log file name) were it comes from.

## Requirements

This script requires NGINX logs split per onionsite, which can be configured
with the following [Onionspray][] configuration line:

    set log_separate 1

## Standalone usage

Chek the script invocation:

    ./onionspray-log-parser --help

The script supports:

* Custom log folder.
* Compressed or uncompressed log folder (but not both at the same time).
* Page hit pattern match to filter results.

## Library usage

Check the [OnionsprayLogParser class](packages/onionspray_log_parser/main.py).

By default it compiles simple page hits statistics for each site, but it can be
easily extended for custom use cases.

## SSH log sync script

The [onionspray-get-logs-from-remotes](onionspray-get-logs-from-remotes) script is also provided
for copying all log files from remote servers into a single folder using SSH.

It can be customized using a `onionspray-get-logs-from-remotes.conf` file (check the
[sample](onionspray-get-logs-from-remotes.conf.sample) for details).

## S3 log sync scripts

This repository also provides two sync scripts
to get logs from [AWS S3](https://en.wikipedia.org/wiki/Amazon_S3):

* [onionspray-get-logs-from-s3][], using the [AWS CLI][].
* [onionspray-get-logs-from-s3fs][], using [s3fs-fuse][].

[onionspray-get-logs-from-s3fs]: onionspray-get-logs-from-s3fs
[onionspray-get-logs-from-s3]: onionspray-get-logs-from-s3

[AWS CLI]: https://aws.amazon.com/cli/
[s3fs-fuse]: https://github.com/s3fs-fuse/s3fs-fuse

To use it, make sure to copy and customize it's corresponding sample config
files:

* [onionspray-get-logs.conf-from-s3.conf.sample](onionspray-get-logs.conf-from-s3.conf.sample) as
`onionspray-get-logs.conf-from-s3.conf`.
* [onionspray-get-logs.conf-from-s3fs.conf.sample](onionspray-get-logs.conf-from-s3fs.conf.sample) as
`onionspray-get-logs.conf-from-s3fs.conf`.

Then you can fetch logs from as many instances as you can, placing each bucket
in a subfolder from a common log folder. Then pass this main log folder as an
argument to `onionspray-log-parser.py` so all files are processed.

Note that [onionspray-get-logs-from-s3fs][] tends to be much slower than
[onionspray-get-logs-from-s3][]:

* [Copy from S3 bucket is slow · Issue #2294 · s3fs-fuse/s3fs-fuse](https://github.com/s3fs-fuse/s3fs-fuse/issues/2294)
* [s3fs is order of magnitude slower in scanning directory tree than direct s3 access · Issue #2193 · s3fs-fuse/s3fs-fuse](https://github.com/s3fs-fuse/s3fs-fuse/issues/2193)
