# Onionspray Log Parser ChangeLog

## v2.1.0 - 2024-05-17

* Support for selecting a single month in the `onionspray-get-logs-from-s3` script
  by using the `MONTH` environment variable.

## v2.0.0 - 2024-04-02

* Support for output templates and a summary table:
  https://gitlab.torproject.org/tpo/onion-services/onionspray-log-parser/-/issues/10

* Breaking changes:
  * Variables `hits`, `dates`, `circuits` and `matches` moved to a `metrics`
    dictionary at the `OnionsprayLogParser` class.

  * Key `site` renamed to `onionsite` in the `hits`, `dates` and `circuits` metrics.

  * Method `update_stats` renamed to `update_metrics`.

## v1.0.0 - 2024-03-27

* Breaking change: variable `stats` moved to `hits`.
  Please update your scripts if you rely on it.

* Fixed stats initialization.

* Adds support for parsing Onion Service circuit IDs
  ([tpo/onion-services/onionspray-log-parser#8][]). This will only make sense
  if your logs contains circuit information (controlled by Onionspray
  `tor_export_circuit_id` setting), otherwise only a single `unix` address will
  appear (corresponding to the socket connection).

* CSV output now accounts for total distinct circuits. It will fallback to the
  number of page hits if your logs does not contain circuit ID information.

[tpo/onion-services/onionspray-log-parser#8]: https://gitlab.torproject.org/tpo/onion-services/onionspray-log-parser/-/issues/8

## v0.0.1 - 2024-03-27

* Project renamed from `eotk-log-parser` to `onionspray-log-parser`. Scripts,
  classes, configuration files etc renamed accordingly.

* Improved verbosity.

* Added examples.

## v0.0.0 - 2024-01-22

* Initial version as `eotk-log-parser`.
