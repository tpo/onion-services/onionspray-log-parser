Reporting
---------

Total:

* Page hits: {{ metrics['matches']['page_hits'] }}
* Non-page hits: {{ metrics['matches']['non_page_hits'] }}
* Ignored User Agents: {{ metrics['matches']['user_agents_ignored'] }}

Summary:

{{ summary }}
