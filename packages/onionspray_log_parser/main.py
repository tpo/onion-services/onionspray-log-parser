#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Onionspray log parser.
#
# Copyright (C) 2022 Silvio Rhatto <rhatto@torproject.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License,
# or any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies
import argparse
import os
import re
from importlib import import_module
from pathlib   import Path
from datetime  import datetime

# Program name
name = "Onionspray Log Parser"

# Program version
version = '0.0.1'

# Log entry pattern
# Adapted from https://gist.github.com/hreeder/f1ffe1408d296ce0591d
# Tested at https://pythex.org/?regex=(%3FP%3Corigin%3E.*)%20-%20-%20%5C%5B(%3FP%3Cdateandtime%3E%5Cd%7B2%7D%5C%2F%5Ba-z%5D%7B3%7D%5C%2F%5Cd%7B4%7D%3A%5Cd%7B2%7D%3A%5Cd%7B2%7D%3A%5Cd%7B2%7D%20(%5C%2B%7C%5C-)%5Cd%7B4%7D)%5C%5D%20((%5C%22(GET%7CPOST)%20)(%3FP%3Cpath%3E.%2B)(http%5C%2F1%5C.1%22))%20(%3FP%3Cstatuscode%3E%5Cd%7B3%7D)%20(%3FP%3Cbytessent%3E%5Cd%2B)%20%5C%22(%3FP%3Curl%3E%5B%5E%5C%22%5D%2B)%5C%22%20%5C%22(%3FP%3Cuseragent%3E%5B%5E%5C%22%5D%2B)%5C%22&test_string=unix%3A%20-%20-%20%5B23%2FMay%2F2022%3A11%3A05%3A17%20%2B0000%5D%20%22GET%20%2Fsome%2Fpath.html%20HTTP%2F1.1%22%20301%200%20%22http%3A%2F%2Fihpiuhiqwudhqpiuhiuhqwiuhdpqiwuhdpiquhdqwiuhiauauauauaua.onion%2Fsome%2Fpath.html%22%20%22Mozilla%2F5.0%20(Windows%20NT%206.2%3B%20WOW64)%20AppleWebKit%2F537.31%20(KHTML%2C%20like%20Gecko)%20Chrome%2F123.4.567.89%20Safari%2F123.45%22%0Aunix%3A%20-%20-%20%5B01%2FJun%2F2022%3A00%3A05%3A07%20%2B0000%5D%20%22GET%20%2F%20HTTP%2F1.1%22%20200%20170257%20%22-%22%20%22python-requests%2F2.25.1%22%0Afc00%3Adead%3Abeef%3A4dad%3A%3A0%3A30%20-%20-%20%5B11%2FJan%2F2024%3A17%3A08%3A43%20%2B0000%5D%20%22GET%20%2Fstatic%2Ffonts%2Ffontawesome%2Fwebfonts%2Ffa-brands-400.woff2%20HTTP%2F1.1%22%20200%2073936%20%22-%22%20%22Mozilla%2F5.0%20(Windows%20NT%2010.0%3B%20rv%3A109.0)%20Gecko%2F20100101%20Firefox%2F115.0%22&ignorecase=1&multiline=0&dotall=0&verbose=0
log_regex = re.compile(
        r"(?P<origin>.*) - - \[(?P<dateandtime>\d{2}\/[a-z]{3}\/\d{4}:\d{2}:\d{2}:\d{2} (\+|\-)\d{4})\] ((\"(GET|POST) )(?P<path>.+)(http\/1\.1\")) (?P<statuscode>\d{3}) (?P<bytessent>\d+) \"(?P<url>[^\"]+)\" \"(?P<useragent>[^\"]+)\"",
        re.IGNORECASE
        )

# Log file name pattern
file_regex = re.compile(r"\.log.*")

# Page hit pattern
# Paths ending with .htm, .html, .shtm, .shtml, .pdf, .php, .asp, .aspx, / or without extension
default_pagehit_pattern = r"(.*\.s?html?$)|(.*\.pdf$)|(.*\.php$)|(.*\.aspx?$)|(/$)|(/[^\.]*$)"

# User-Agent ignore pattern
default_ignore_user_agent_pattern = r"python-requests/([0-9]*.){3}"

# Date pattern
date_format = "%d/%b/%Y:%H:%M:%S %z"

# Default extension for the logfiles to search
default_extension = 'gz'

# Default pattern
default_log_file_pattern = '*'

# Default path where to search logs
default_path = 'logs'

# Default path for the CSV output
default_csv_output_file = None

# Default template
default_template = None

class OnionsprayLogParser():
    """
    Parses Onionspray logs.
    """

    def __init__(
            self,
            path=default_path,
            extension=default_extension,
            ignore_user_agent_pattern=default_ignore_user_agent_pattern,
            pagehit_pattern=default_pagehit_pattern,
            csv_output_file=default_csv_output_file,
            log_file_pattern=default_log_file_pattern,
            template=default_template,
            ):
        """
        Class initialization.
        """

        if not os.path.exists(path):
            raise FileNotFoundError(path)

        self.folder    = Path(path)
        self.logfiles  = list(self.folder.glob('**/' + log_file_pattern + '.' + extension))
        self.extension = extension
        self.metrics   = {
                'hits'     : {},
                'dates'    : {},
                'circuits' : {},
                'matches'  : {
                    'page_hits'          : 0,
                    'non_page_hits'      : 0,
                    'user_agents_ignored': 0,
                    },
                }

        if ignore_user_agent_pattern is not None or ignore_user_agent_pattern != '':
            self.ignore_user_agent_regex = re.compile(ignore_user_agent_pattern)
        else:
            self.ignore_user_agent_regex = None

        if pagehit_pattern is not None or pagehit_pattern != '':
            self.pagehit_regex = re.compile(pagehit_pattern)
        else:
            self.pagehit_regex = None

        if csv_output_file != None:
            self.csv_output_file = csv_output_file

        if template != None:
            if not os.path.exists(template):
                raise FileNotFoundError(template)

            self.template = template

        self.log(name + ' ' + version)
        self.log('===========================')
        self.log('')

        self.log('Configuration')
        self.log('-------------')
        self.log('')
        self.log('Folder: '                    + str(path))
        self.log('Log file name pattern: '     + str(log_file_pattern))
        self.log('Extension: '                 + str(extension))
        self.log('Ignore User-Agent pattern: ' + str(ignore_user_agent_pattern))
        self.log('Page Hit pattern: '          + str(pagehit_pattern))
        self.log('CSV output: '                + str(csv_output_file))
        self.log('Report template: '           + str(template))
        self.log('')

    def run(self):
        """
        Main loop.
        """

        self.log('Processing')
        self.log('----------')
        self.log('')

        for logfile in self.logfiles:
            if logfile.is_file():
                self.log('Processing logfile ' + str(logfile) + '...')
                self.process_logfile(logfile)

        self.log('')

        self.metrics['summary_table'] = self.build_summary_table()

        if 'csv_output_file' in dir(self):
            self.write_csv()

    def build_summary_table(self):
        header = ['Month', 'Onionsite', 'Page hits', 'Circuits']
        body   = [ ]

        for year in self.metrics['hits']:
            for month in self.metrics['hits'][year]:
                for onionsite in self.metrics['hits'][year][month]:
                    date = '0' + str(month) if month <= 9 else str(month)
                    date = str(year) + '-' + date

                    body.append([
                            date,
                            onionsite,
                            self.metrics['hits'][year][month][onionsite],
                            len(self.metrics['circuits'][year][month][onionsite])
                            ])

        return (header, body)

    def write_csv(self):
        """
        Write the CSV output.
        """

        import csv

        with open(self.csv_output_file, 'w', newline='') as csvfile:
            csvwriter = csv.writer(csvfile, delimiter=',', quotechar='"',
                    quoting=csv.QUOTE_MINIMAL)

            (header, body) = self.metrics['summary_table']

            csvwriter.writerow(header)

            for item in body:
                csvwriter.writerow(item)

    # Inspired by https://stackoverflow.com/a/9536084
    def tabulate(self, header, data):
        row_format ="{:>20}" * (len(header))
        output     = row_format.format(*header) + "\n"

        for row in data:
            output += row_format.format(*row) + "\n"

        return output

    def log(self, message, level='info'):
        """
        Simple log wrapper.
        """

        print(message)

    def process_logfile(self, filename):
        """
        Process each log file.
        """

        module = self.extension

        if self.extension == 'gz':
            module = 'gzip'
        elif self.extension == 'bz2':
            module = 'bz2'
        # Does not have the 'rt' opening mode
        #elif self.extension == 'zip':
        #    module = 'zipfile'
        elif self.extension == 'xz' or self.extension == 'lzma':
            module = 'lzma'
        else:
            module = None

        if module is not None:
            library   = import_module(module)
            open_func = library.open
        else:
            open_func = open

        with open_func(filename, 'rt') as decompressed_file:
            for line in decompressed_file.readlines():
                parsed = log_regex.search(line)

                if parsed:
                    data = parsed.groupdict()

                    self.update_metrics(data, str(filename))

    def update_metrics(self, data, filename):
        """
        Update statistics.
        """

        date      = self.parse_date(data['dateandtime'])
        onionsite = os.path.basename(filename).replace('nginx-access-', '')
        onionsite = file_regex.sub('', onionsite)
        origin    = data['origin']

        # User-Agent check
        if self.ignore_user_agent_regex != None and \
                self.ignore_user_agent_regex.match(data['useragent']):
            self.metrics['matches']['user_agents_ignored'] += 1

            return

        # Page hit check
        if self.pagehit_regex != None and not self.pagehit_regex.match(data['path']):
            self.metrics['matches']['non_page_hits'] += 1

            return

        self.metrics['matches']['page_hits'] += 1

        #
        # Hits
        #

        if date.year not in self.metrics['hits']:
            self.metrics['hits'][date.year] = {
                    date.month: {
                        onionsite: 0,
                        },
                    }

        elif date.month not in self.metrics['hits'][date.year]:
            self.metrics['hits'][date.year][date.month] = {
                    onionsite: 0,
                    }

        elif onionsite not in self.metrics['hits'][date.year][date.month]:
            self.metrics['hits'][date.year][date.month][onionsite] = 0

        self.metrics['hits'][date.year][date.month][onionsite] = self.metrics['hits'][date.year][date.month][onionsite] + 1

        #
        # Dates
        #

        if onionsite not in self.metrics['dates']:
            self.metrics['dates'][onionsite] = {
                    'earliest_record': date,
                    'latest_record'  : date,
                    }

        if self.metrics['dates'][onionsite]['earliest_record'] > date:
            self.metrics['dates'][onionsite]['earliest_record'] = date

        if self.metrics['dates'][onionsite]['latest_record'] < date:
            self.metrics['dates'][onionsite]['latest_record'] = date

        #
        # Circuits
        #

        if date.year not in self.metrics['circuits']:
            self.metrics['circuits'][date.year] = {
                    date.month: {
                        onionsite: {
                            origin: 0,
                            }
                        },
                    }

        elif date.month not in self.metrics['circuits'][date.year]:
            self.metrics['circuits'][date.year][date.month] = {
                    onionsite: {
                        origin: 0,
                        }
                    }

        elif onionsite not in self.metrics['circuits'][date.year][date.month]:
            self.metrics['circuits'][date.year][date.month][onionsite] = {
                    origin: 0,
                    }

        elif origin not in self.metrics['circuits'][date.year][date.month][onionsite]:
            self.metrics['circuits'][date.year][date.month][onionsite][origin] = 0

        self.metrics['circuits'][date.year][date.month][onionsite][origin] = self.metrics['circuits'][date.year][date.month][onionsite][origin] + 1

    def parse_date(self, dateantime):
        """
        Parse a date.
        """

        date = datetime.strptime(dateantime, date_format)

        return date

    def report(self, stdout=False):
        # Create a summary table
        (header, body) = self.build_summary_table()

        if 'template' in dir(self):
            output = self.report_from_template()

        else:
            # Use pretty printing to display results
            from pprint import pformat

            summary = self.tabulate(*self.metrics['summary_table'])

            output  = ''
            output += "Reporting\n"
            output += "---------\n\n"

            # Display page hits by onionsite
            if bool(self.metrics['hits']):
                output += "Hits by onionsite:\n\n"
                output += pformat(self.metrics['hits'])
                output += "\n\n"

            # Display page hits by onionsite and circuit
            if bool(self.metrics['circuits']):
                output += "Hits by onionsite and circuit:\n\n"
                output += pformat(self.metrics['circuits'])
                output += "\n\n"

            # Display dates
            if bool(self.metrics['dates']):
                output += "Dates by onionsite:\n\n"
                output += pformat(self.metrics['dates'])
                output += "\n\n"

            output += "Total:\n\n"
            output += pformat(self.metrics['matches'])
            output += "\n\n"

            output += "Summary:\n\n"
            output += summary

        if stdout is True:
            print(output)

        return output

    def report_from_template(self):
        from jinja2 import Environment, FileSystemLoader, select_autoescape

        env = Environment(
                loader     = FileSystemLoader(os.path.dirname(self.template)),
                autoescape = select_autoescape()
                )

        template = env.get_template(os.path.basename(self.template))

        return template.render(
                metrics                 = self.metrics,
                date                    = datetime.now().strftime('%Y-%m-%d'),
                summary                 = self.tabulate(*self.metrics['summary_table']),
                pagehit_regex           = str(self.pagehit_regex),
                ignore_user_agent_regex = str(self.ignore_user_agent_regex),
                )

def cmdline_parser():
    """
    Generate command line arguments

    :rtype: argparse.ArgumentParser
    :return: The parser object
    """

    description = 'Parses Onionspray webserver logfiles'
    parser      = argparse.ArgumentParser(
                    description=description,
                    formatter_class=argparse.RawDescriptionHelpFormatter,
                  )

    parser.add_argument('-p', '--path',
            default=default_path,
            help="Folder where to search for logs (default: \"%(default)s\")")

    parser.add_argument('-l', '--log_file_pattern',
            default=default_log_file_pattern,
            help="""
                Log file name pattern, without the extension (default: \"%(default)s\")
                Example: '*202210*' to consider only logs containing 202210 on their file names.
                """
            )

    parser.add_argument('-e', '--extension',
            default=default_extension,
            help="""
                Log file name extensions to search for (default: \"%(default)s\")
                (supported: \"gz\", \"bz2\", \"xz\", \"lzma\"; if other extension
                is passed, it's assumed the plaintext format)"""
            )

    parser.add_argument('-i', '--pagehit_pattern',
            default=default_pagehit_pattern,
            help="Page hit pattern (default: \"%(default)s\")")

    parser.add_argument('-u', '--ignore_user_agent_pattern',
            default=default_ignore_user_agent_pattern,
            help="Ignore User-Agent pattern (default: \"%(default)s\")")

    parser.add_argument('-c', '--csv_output_file',
            default=default_csv_output_file,
            help="Output results in the given CSV file (default: \"%(default)s\")")

    parser.add_argument('-t', '--template',
            default=default_template,
            help="Report template file used for printing results (default: \"%(default)s\")")

    return parser

def cmdline():
    """
    Evalutate the command line.

    :rtype: argparse.Namespace
    :return: Command line arguments.
    """

    parser = cmdline_parser()
    args   = parser.parse_args()

    return args
